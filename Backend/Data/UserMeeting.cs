using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


//junction between user and meeting

namespace Backend.Data
{
    public class UserMeeting
    {
        public int MeetingID { get; set; }

        public Backend.Data.Meeting Meeting { get; set; }

        public int UserID { get; set; }

        public User User { get; set; }
    }
}