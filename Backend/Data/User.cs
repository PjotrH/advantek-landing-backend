using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BackEnd.Data;

namespace Backend.Data
{

    public class User : AdvantekDTO.Models.User{

    public virtual IList<UserMeeting> UserMeetings { get; set; }

    }

}