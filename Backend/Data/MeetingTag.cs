using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdvantekDTO.Models;


//junction between tag and meeting

namespace BackEnd.Data
{
    public class MeetingTag
    {
        
        public int MeetingID { get; set; }

        public Backend.Data.Meeting Meeting { get; set; }


        public int TagID { get; set; }

        public Backend.Data.Tag Tag { get; set; }
    }
}