using System.Collections.Generic;
using Backend.Data;

namespace Backend.DataModels
 {
     public class TagResponse : Tag
     {
         // TODO: Set order of JSON properties so this shows up last not first
         public ICollection<Tag> Tags { get; set; } = new List<Tag>();
     }
 }