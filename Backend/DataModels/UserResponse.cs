 using System;
 using System.Collections.Generic;
 using System.Text;
using Backend.Data;

 namespace Backend.DataModels
 {
     public class UserResponse : Backend.Data.User
     {
         // TODO: Set order of JSON properties so this shows up last not first
         public ICollection<User> Users { get; set; } = new List<User>();
     }
 }