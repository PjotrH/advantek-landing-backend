 using System;
 using System.Collections.Generic;
 using System.Text;
using Backend.Data;

 namespace Backend.DataModels
 {
     public class MeetingResponse : Backend.Data.Meeting
     {
         // TODO: Set order of JSON properties so this shows up last not first
         public ICollection<Meeting> Meetings { get; set; } = new List<Meeting>();
     }
 }